#ifndef leds_h
#define leds_h
#include "delay.h"
#include "MKL05Z4.h"   /* Device header */

#define ledGreen 0  //Green led is first in led_mask
#define ledRed 1  //Red led is second in led_mask


void ledsInit(void);

void ledgreenBlink(uint32_t x, uint32_t y);
void ledredBlink(uint32_t x, uint32_t y);
void ledblueBlink(uint32_t x, uint32_t y);
void ledsOff (void);
void ledsOn (void);

#endif
