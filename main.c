#include "MKL05Z4.h"
#include <stdio.h>
#include <stdbool.h>
#include "buttons.h"
#include "leds.h"
#include "sequence.h"
#include "lcd.h"
#include "delay.h"

int main(void){
	buttonsInit();
	ledsInit();
	LCD_Initalize();
	LCD_WriteText("PIN: ");
	while (1){
		checkButtons();
	}
}
