#ifndef sequence_h
#define sequence_h

#include "MKL05Z4.h"   /* Device header */

void checkButtons(void);
void setButton(uint8_t button);
void selectedButton(uint8_t button);
void check(void);
void reset(void);
void reset_counter(void);


#endif