#ifndef delay_h
#define delay_h

#include "MKL05Z4.h"   /* Device header */

void delay_mc(uint32_t value);
#endif