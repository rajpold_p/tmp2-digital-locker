#include "MKL05Z4.h"
#include "delay.h"

void ledsInit(void){
	
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK; 
	PORTB->PCR[8] = PORT_PCR_MUX(1UL);  /* Pin PTB8 is GPIO */
	PORTB->PCR[9] = PORT_PCR_MUX(1UL);  /* Pin PTB9 is GPIO */
	PORTB->PCR[10] = PORT_PCR_MUX(1UL);	/* Pin PTB10 is GPIO */
	
	PTB->PDDR |= 1<<8 | 1<<9 | 1<<10;  /* enable PTB8 as Output */

	PTB->PSOR = 1<<8 | 1<<9 | 1<<10;  /* switch LEDs off */
}

void ledsOn(void) {
		PTB->PCOR = 1<<8 | 1<<9 | 1<<10; 
}

void ledsOff(void) {
		PTB->PSOR = 1<<8 ;//| 1<<9 | 1<<10; 
}


void ledredBlink(uint32_t x, uint32_t y){
	uint32_t count;
	
	for(count = 0; count < x; count++){	
		PTB->PCOR = 1<<8;
		delay_mc(y);		
		PTB->PSOR = 1<<8;
		delay_mc(y);	
	}
}

void ledgreenBlink(uint32_t x, uint32_t y){
	uint32_t count;
	
	for(count = 0; count < x; count++){	
		PTB->PCOR = 1<<9;
		delay_mc(y);		
		PTB->PSOR = 1<<9;
		delay_mc(y);	
	}
}

void ledblueBlink(uint32_t x, uint32_t y){
	uint32_t count;
	
	for(count = 0; count < x; count++){	
		PTB->PCOR = 1<<10;
		delay_mc(y);		
		PTB->PSOR = 1<<10;
		delay_mc(y);	
	}
}
