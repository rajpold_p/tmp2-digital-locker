#ifndef buttons_h
#define buttons_h

#include "MKL05Z4.h"

#define row1 (1UL<<11)	//PTB11
#define row2 (1UL<<11)//PTA11
#define row3 (1UL<<1)	//PTB1
#define row4 (1UL<<2)	//PTB2

#define column1 (1<<10)	//PTA10
#define column2 (1<<12)	//PTA12
#define column3 (1<<6)	//PTB6
#define column4 (1<<7)	//PTB7

void buttonsInit(void);
uint32_t column1Read(void);
uint32_t column2Read(void);
uint32_t column3Read(void);
uint32_t column4Read(void);
void row1Dis(void);
void row2Dis(void);
void row3Dis(void);
void row4Dis(void);
void row1En(void);
void row2En(void);
void row3En(void);
void row4En(void);

#endif
