#include "MKL05Z4.h"
#include "lcd.h"
#include "delay.h"

//-------------------------------------------------------------------------------------------------
// Wy?wietlacz alfanumeryczny ze sterownikiem HD44780
// Sterowanie w trybie 4-bitowym bez odczytu flagi zaj?to?ci
// z dowolnym przypisaniem sygna??w steruj?cych
// Plik : HD44780.c	
// Mikrokontroler : Atmel AVR
// Kompilator : avr-gcc
// Autor : Rados?aw Kwiecie?
// ?r?d?o : http://radzio.dxp.pl/hd44780/
// Data : 24.03.2007
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//
// Funkcja wystawiaj?ca p?bajt na magistral? danych
//
//-------------------------------------------------------------------------------------------------
void _LCD_OutNibble(unsigned char nibbleToWrite)
{
	if(nibbleToWrite & 0x01)
		PTA->PSOR = LCD_DB4;
	else
		PTA->PCOR = LCD_DB4;

	if(nibbleToWrite & 0x02)
		PTA->PSOR = LCD_DB5;
	else
		PTA->PCOR = LCD_DB5;

	if(nibbleToWrite & 0x04)
		PTB->PSOR = LCD_DB6;
	else
		PTB->PCOR = LCD_DB6;

	if(nibbleToWrite & 0x08)
		PTB->PSOR = LCD_DB7;
	else
		PTB->PCOR = LCD_DB7;
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu bajtu do wy?wietacza (bez rozr?nienia instrukcja/dane).
//
//-------------------------------------------------------------------------------------------------
void _LCD_Write(unsigned char dataToWrite)
{
	PTA->PSOR = LCD_E;
	_LCD_OutNibble(dataToWrite >> 4);
	PTA->PCOR = LCD_E;
	PTA->PSOR = LCD_E;
	_LCD_OutNibble(dataToWrite);
	PTA->PCOR = LCD_E;		
	delay_mc(1);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu rozkazu do wy?wietlacza
//
//-------------------------------------------------------------------------------------------------
void LCD_WriteCommand(unsigned char commandToWrite)
{
PTA->PCOR = LCD_RS;
_LCD_Write(commandToWrite);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu danych do pami?ci wy?wietlacza
//
//-------------------------------------------------------------------------------------------------
void LCD_WriteData(unsigned char dataToWrite)
{
PTA->PSOR = LCD_RS;
_LCD_Write(dataToWrite);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja wy?wietlenia napisu na wyswietlaczu.
//
//-------------------------------------------------------------------------------------------------
void LCD_WriteText(char * text)
{
while(*text)
  LCD_WriteData(*text++);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja ustawienia wsp?rz?dnych ekranowych
//
//-------------------------------------------------------------------------------------------------
void LCD_GoTo(unsigned char x, unsigned char y)
{
	LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja czyszczenia ekranu wy?wietlacza.
//
//-------------------------------------------------------------------------------------------------
void LCD_Clear(void)
{
	LCD_WriteCommand(HD44780_CLEAR);
	delay_mc(2);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja przywr?cenia pocz?tkowych wsp?rz?dnych wy?wietlacza.
//
//-------------------------------------------------------------------------------------------------
void LCD_Home(void)
{
	LCD_WriteCommand(HD44780_HOME);
	delay_mc(2);
}
//-------------------------------------------------------------------------------------------------
//
// Procedura inicjalizacji kontrolera HD44780.
//
//-------------------------------------------------------------------------------------------------
void LCD_Initalize(void)
{
unsigned char i;
SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK | SIM_SCGC5_PORTB_MASK;

PORTB->PCR[8] |= PORT_PCR_MUX(1UL);			//PTB8 - D7
PORTB->PCR[9] |= PORT_PCR_MUX(1UL);			//PTB9 - D6
PORTA->PCR[8] |= PORT_PCR_MUX(1UL);		//PTA8 - D5
PORTA->PCR[9] |= PORT_PCR_MUX(1UL);		//PTA9 - D4		
PORTA->PCR[5] |= PORT_PCR_MUX(1UL);			//PTA5 - RS
PORTA->PCR[7] |= PORT_PCR_MUX(1UL);		//PTA7 - E

//ustawienie port�w jako wyjscia
PTB->PDDR |= LCD_DB7;			//PTB8 - D7
PTB->PDDR |= LCD_DB6;			//PTB9 - D6
PTA->PDDR |= LCD_DB5;		//PTA8 - D5
PTA->PDDR |= LCD_DB4;		//PTA9 - D4		
PTA->PDDR |= LCD_RS;			//PTA5 - RS
PTA->PDDR |= LCD_E;		//PTA7 - E
	
delay_mc(15); // oczekiwanie na ustalibizowanie sie napiecia zasilajacego
PTA->PCOR = LCD_RS; // wyzerowanie linii RS
PTA->PCOR = LCD_E;  // wyzerowanie linii E

for(i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
  {
  PTA->PSOR = LCD_E; //  E = 1
  _LCD_OutNibble(0x03); // tryb 8-bitowy
  PTA->PCOR = LCD_E; // E = 0
  delay_mc(5); // czekaj 5ms
  }

PTA->PSOR = LCD_E; // E = 1
_LCD_OutNibble(0x02); // tryb 4-bitowy
PTA->PCOR = LCD_E; // E = 0

delay_mc(1); // czekaj 1ms 
LCD_WriteCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wy??czenie wyswietlacza
LCD_WriteCommand(HD44780_CLEAR); // czyszczenie zawartos?i pamieci DDRAM
delay_mc(2);
LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);// inkrementaja adresu i przesuwanie kursora
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_ON | HD44780_CURSOR_BLINK); // w??cz LCD, bez kursora i mrugania
}

//-------------------------------------------------------------------------------------------------
//
// Koniec pliku HD44780.c
//
//-------------------------------------------------------------------------------------------------
