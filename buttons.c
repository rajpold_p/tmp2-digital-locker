#include "MKL05Z4.h"

#include "buttons.h"

void buttonsInit (void)
{
	//Podpiecie zegara
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK | SIM_SCGC5_PORTB_MASK;
	//Podpiecie pin?w

	PORTB->PCR[7] |= PORT_PCR_MUX(1UL);			//PTB7 - 8
	PORTB->PCR[6] |= PORT_PCR_MUX(1UL);			//PTB6 - 7
	PORTA->PCR[12] |= PORT_PCR_MUX(1UL);		//PTA12 - 6
	PORTA->PCR[10] |= PORT_PCR_MUX(1UL);		//PTA10 - 5
		
	PORTB->PCR[11] |= PORT_PCR_MUX(1UL);			//PTB11 - 2
	PORTA->PCR[11] |= PORT_PCR_MUX(1UL);		//PTA11 - 3
	PORTB->PCR[1] |= PORT_PCR_MUX(1UL);			//PTB1 - 2
	PORTB->PCR[2] |= PORT_PCR_MUX(1UL);			//PTB2 - 1

	//Ustawianie pin?w jako wyjscia
	PTB->PDDR |= row1;
	PTA->PDDR |= row2;
	PTB->PDDR |= row3;
	PTB->PDDR |= row4;
	
	//Ustawienie pin�w jako wejscia
	PTA->PDDR	&= ~column1;
	PTA->PDDR &= ~column2;
	PTB->PDDR &= ~column3;
	PTB->PDDR &= ~column4;
	
	//Podciagniecie rezystorow
	PORTA->PCR[10] |= PORT_PCR_PE_MASK;
	PORTA->PCR[12] |= PORT_PCR_PE_MASK;
	PORTB->PCR[6] |= PORT_PCR_PE_MASK;
	PORTB->PCR[7] |= PORT_PCR_PE_MASK;	
//	PORTA->PCR[11] |= PORT_PCR_PE_MASK;
//	PORTB->PCR[1] |= PORT_PCR_PE_MASK;
//	PORTB->PCR[2] |= PORT_PCR_PE_MASK;
//	PORTB->PCR[5] |= PORT_PCR_PE_MASK;

}


uint32_t column1Read(void){
		return PTA->PDIR & column1;
}

uint32_t column2Read(void){
		return PTA->PDIR & column2;
}

uint32_t column3Read(void){
		return PTB->PDIR & column3;
}

uint32_t column4Read(void){
		return PTB->PDIR & column4;
}

void row1Dis(void){
		PTB->PSOR = row1;
}

void row2Dis(void){
	  PTA->PSOR = row2;
}

void row3Dis(void){
		PTB->PSOR = row3;
}

void row4Dis(void){
		PTB->PSOR = row4;
}
void row1En(void){
		PTB->PCOR = row1;
}

void row2En(void){
	  PTA->PCOR = row2;
}

void row3En(void){
		PTB->PCOR = row3;
}

void row4En(void){
		PTB->PCOR = row4;
}

