#include "MKL05Z4.h"
#include <stdio.h>
#include <stdbool.h>
#include "buttons.h"
#include "leds.h"
#include "delay.h"
#include "lcd.h"


uint8_t timerButtons[16];
uint8_t lock_pin[4] = {1,2,3,5}; // it is 1 2 3 4
uint8_t pre_lock_pin[4] = {15,15,15,15};
uint8_t entered_pin[4] = {13,13,13,13};
uint8_t admin_pin[4] = {1,1,1,1};

uint8_t stage = 0;
uint8_t error = 0;
uint8_t processButton = 0;

bool test = false;
int counter = 0;
bool adminMode = false;

void setButton(uint8_t button);
void selectedButton(uint8_t button);
void check();
void reset(void);
void reset_counter(void);


void checkButtons(){
		
    if (processButton == 0){
        row1En();
				row2Dis();
				row3Dis();
				row4Dis();
        processButton = 1;
    }
    else if (processButton == 1){
        if (!column1Read()) setButton(1);
        if (!column2Read()) setButton(2);
        if (!column3Read()) setButton(3);
        if (!column4Read()) setButton(4);
        processButton = 2;
    }
    else if (processButton == 2){
        row1Dis();
				row2En();
				row3Dis();
				row4Dis();
        processButton = 3;
    }
    else if (processButton == 3){
        if (!column1Read()) setButton(5);
        if (!column2Read()) setButton(6);
        if (!column3Read()) setButton(7);
        if (!column4Read()) setButton(8);
        processButton = 4;
    }
    else if (processButton == 4){
        row1Dis();
				row2Dis();
				row3En();
				row4Dis();
        processButton = 5;
    }
    else if (processButton == 5){
        if (!column1Read()) setButton(9);
        if (!column2Read()) setButton(10);
        if (!column3Read()) setButton(11);
        if (!column4Read()) setButton(12);
        processButton = 6;
    }
    else if (processButton == 6){
        row1Dis();
				row2Dis();
				row3Dis();
				row4En();
        processButton = 7;
    }
    else if (processButton == 7){
        if (!column1Read()) setButton(13);
        if (!column2Read()) setButton(14);
        if (!column3Read()) setButton(15);
        if (!column4Read()) setButton(16);
        processButton = 0;
    }
    setButton(0);
}

void setButton(uint8_t button){

    if (button!= 0) timerButtons[button-1] = 20;
    for(uint8_t i = 0; i<16;i++){
        if (timerButtons[i] > 0) timerButtons[i] --;
        if (timerButtons[i] == 1){
            selectedButton(i+1);
        }
    }
}

void selectedButton(uint8_t button){

    if (button== 12){
        reset();
    }

    else if ((button== 1 || button== 2 || button== 3 || button== 5 || button== 6 || button== 7 || button== 9 || button== 10 || button== 11 || button== 14) && stage < 4){

        if (stage == 0) {
						if (adminMode==false){
							entered_pin[stage] = button;
						}
						else if (adminMode==true){
							pre_lock_pin[stage] = button;
						}
						LCD_WriteText("*");
						stage++;
        }
        else if (stage == 1) {
         		if (adminMode==false){
							entered_pin[stage] = button;
						}
						else if (adminMode==true){
							pre_lock_pin[stage] = button;
						}
						LCD_WriteText("*");
						stage++;
        }
        else if (stage == 2) {
        		if (adminMode==false){
							entered_pin[stage] = button;
						}
						else if (adminMode==true){
							pre_lock_pin[stage] = button;
						}
						LCD_WriteText("*");
						stage++;
        }
        else if (stage == 3) {
          	if (adminMode==false){
							entered_pin[stage] = button;
						}
						else if (adminMode==true){
							pre_lock_pin[stage] = button;
						}
						LCD_WriteText("*");
						stage++;
        }
    }

    else if (button== 13 || button== 15 || button== 16) {
        // do nothing
    }

    else if (button== 8 && stage > 0 && stage < 5){
        stage--;
        LCD_WriteCommand(HD44780_DISPLAY_CURSOR_SHIFT | HD44780_SHIFT_LEFT );
				LCD_WriteText(" ");
				LCD_WriteCommand(HD44780_DISPLAY_CURSOR_SHIFT | HD44780_SHIFT_LEFT );
    }

    else if (button == 4 && stage == 4){
		
			if(adminMode==false){
					check();
					if(test == true && adminMode==false){
							LCD_Clear();
							LCD_WriteText("CORRECT");
							//ledgreenBlink(5,50);
							delay_mc(5000);
							LCD_Clear();
							//counter_start = true;
					}
					if(test == false && adminMode==false){
							LCD_Clear();
							LCD_WriteText("INCORRECT");
							//ledredBlink(5,50);
							delay_mc(5000);
							LCD_Clear();
							error++;
							if (error>3){
								LCD_WriteText("LOCKED");
								delay_mc(10000);
								error=0;
							}//counter_start = true;
					}
					
			}
			if(adminMode==true && stage == 4){
				LCD_Clear();
				LCD_WriteText("PSSWD CHANGED");
				//ledblueBlink(5,50);
				delay_mc(1000);
				int i = 0;
				for (i=0;i<4;i++){
					lock_pin[i]=pre_lock_pin[i];
				}
				adminMode=false;
			}
			reset();
    }
}


void reset(){
		
			for (int i = 0; i < 4; i++){
				 
					entered_pin[i] = 13;
			}
			stage = 0;
			test = false;
			if (adminMode==false){
				LCD_Clear();
				LCD_WriteText("PIN: ");
			}
			else if (adminMode==true){
				LCD_Clear();
				LCD_WriteText("ADMIN MODE");
				delay_mc(2000);
				LCD_Clear();
				LCD_WriteText("NEW PSWD: ");
			}
}

void check(){
		
		stage++;
		if((entered_pin[0] == admin_pin[0]) && (entered_pin[1] == admin_pin[1]) && (entered_pin[2] == admin_pin[2]) && (entered_pin[3] == admin_pin[3])){
        test = true;
				adminMode=true;
				reset();
		}
		else if((entered_pin[0] == lock_pin[0]) && (entered_pin[1] == lock_pin[1]) && (entered_pin[2] == lock_pin[2]) && (entered_pin[3] == lock_pin[3])){
        test = true;
		}  
}

